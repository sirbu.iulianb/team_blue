package repository;

import model.Gender;
import model.Patient;
import org.junit.Assert;

import org.junit.jupiter.api.Test;
class PatientRepositoryImplTest {

    @Test
    void shouldReturnTrueWhenCreatePatient() {
        Patient patient = new Patient();
        patient.setAddress("Str Noastra");
        patient.setAge(32);
        patient.setFirstName("joi");
        patient.setLastName("vineri");
        patient.setGender(Gender.F);
        patient.setPhone("075463526");
        PatientRepositoryImpl patientRepository = new PatientRepositoryImpl();
        Integer id = patientRepository.save(patient);
        if (id == -1) {
            Assert.assertTrue(false);
        }
        boolean createdSuccessfully = false;
        if (patientRepository.findPatientById(id) != null) {
            createdSuccessfully = true;
        }
        Assert.assertTrue(createdSuccessfully);
    }


    @Test
    void findAll() {
    }

    @Test
    void findPatientById() {
    }

    @Test
    void updatePatient() {
    }

    @Test
    void testUpdatePatient() {
    }

    @Test
    void deletePatient() {
    }
}