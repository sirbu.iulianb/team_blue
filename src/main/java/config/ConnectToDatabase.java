package config;

import model.Consultation;
import model.Dentist;
import model.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.*;

public class ConnectToDatabase {

    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Properties properties = new Properties();
                properties.put(Environment.URL, "jdbc:mysql://localhost:3306/teamblue_stomaproj?serverTimezone=UTC");
                properties.put(Environment.USER, "root");
                properties.put(Environment.PASS, "2411859vanadiu");
                properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                properties.put(Environment.HBM2DDL_AUTO, "update");
                Configuration configuration = new Configuration();
                configuration.setProperties(properties);

                configuration.addAnnotatedClass(Dentist.class);
                configuration.addAnnotatedClass(Patient.class);
                configuration.addAnnotatedClass(Consultation.class);

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                this.sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return sessionFactory;
    }


    private Session session;
    private Transaction transaction;

    public Session getSession() {
        return session;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public ConnectToDatabase invoke() {
        ConnectToDatabase connect = new ConnectToDatabase();
        session = connect.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        return this;
    }
}