package model;

import model.Dentist;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "consultations")
public class Consultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer patientId;
    private Integer dentistId;
    private String affliction;
    private LocalDate date;

    @ManyToOne
    @JoinColumn (name = "patientId", insertable = false, updatable = false)
    private Patient patient;

    @ManyToOne
    @JoinColumn (name = "dentistId", insertable = false, updatable = false)
    private Dentist dentist;

    public Consultation(Integer patientId, Integer dentistId, String affliction, LocalDate date) {
        this.patientId = patientId;
        this.dentistId = dentistId;
        this.affliction = affliction;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getDentistId() {
        return dentistId;
    }

    public void setDentistId(Integer dentistId) {
        this.dentistId = dentistId;
    }

    public String getAffliction() {
        return affliction;
    }

    public void setAffliction(String affliction) {
        this.affliction = affliction;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Consultation() {
    }

    @Override
    public String toString() {
        return "model.Consultation{" +
                "id=" + id +
                ", patientId=" + patientId +
                ", dentistId=" + dentistId +
                ", affliction='" + affliction + '\'' +
                ", date=" + date +
                '}';
    }
}
