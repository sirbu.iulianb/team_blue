package model;

import model.Consultation;
import model.Gender;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "patients")

public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;
    private String address;
    private String phone;
    private int[] socialNumber;


    @OneToMany(mappedBy = "id")
    private List<Consultation> consultations;

    public List<Consultation> getConsultations() {
        return consultations;
    }

    public void setConsultations(List<Consultation> consultations) {
        this.consultations = consultations;
    }

    public Patient(String firstName, String lastName, Gender gender, int age, String address, String phone, int[] socialNumber, List<Consultation> consultations) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.address = address;
        this.phone = phone;
        this.socialNumber = socialNumber;
        this.consultations = consultations;
    }


    public boolean validateSocialNumber() {
        if (socialNumber.length == 13) {
            return true;
        }
        return false;
    }

    public void checkGenderFromSocialNumber(){
        for (int i=0; i<socialNumber.length; i++){
            if (socialNumber[0]==1){
                
            }
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Patient(int[] socialNumber) {
        this.socialNumber = socialNumber;
    }

    public int[] getSocialNumber() {
        return socialNumber;
    }

    public void setSocialNumber(int[] socialNumber) {
        this.socialNumber = socialNumber;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", socialNumber='" + socialNumber + '\'' +
                ", consultations=" + consultations +
                '}';
    }

    public Patient() {
    }
}
