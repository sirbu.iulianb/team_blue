package repository;

import config.ConnectToDatabase;
import model.Dentist;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class DentistRepositoryImpl implements DentistRepository {
    @Override
    public void save(Dentist dentist) {
        try {
            ConnectToDatabase connect = new ConnectToDatabase();
            Session session = connect.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.save(dentist);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Dentist> findAll() {
        try {
            ConnectToDatabase connect = new ConnectToDatabase();
            Session session = connect.getSessionFactory().openSession();
            Query<Dentist> query = session.createQuery("from model.Dentist", Dentist.class);
            return query.list();
        } catch (Exception e) {
            System.out.println(e);
        }
        return Collections.emptyList();
    }

    @Override
    public Dentist findDentistById(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Dentist foundDentist = session.find(Dentist.class, id);
           return foundDentist;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }


    @Override
    public void updateDentist(Integer id, Dentist newDentist) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            Dentist oldDentist = session.find(Dentist.class, id);
            oldDentist.setFirstName(newDentist.getFirstName());
            oldDentist.setLastName(newDentist.getLastName());
            oldDentist.setSpecialty(newDentist.getSpecialty());
            connection.getSession().update(newDentist);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    @Override
    public void updateDentist(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Dentist dentist = connection.getSession().find(Dentist.class, id);

            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce new dentist first name:");
            String firstName = scanner.nextLine();
            System.out.println("Introduce new dentist last name:");
            String lastName = scanner.nextLine();
            System.out.println("Select new dentist specialty:");
            String speciality = scanner.next();

            dentist.setFirstName(firstName);
            dentist.setLastName(lastName);
            dentist.setSpecialty(speciality);
            connection.getSession().update(dentist);
            connection.getTransaction().commit();
            connection.getSession().close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteDentist(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            Dentist dentist = session.find(Dentist.class, id);
            session.delete(dentist);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
