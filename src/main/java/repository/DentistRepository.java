package repository;

import model.Dentist;

import java.util.List;

public interface DentistRepository {
    void save(Dentist dentist);

    List<Dentist> findAll();

    Dentist findDentistById(Integer id);

    void updateDentist(Integer id, Dentist newDentist);

    void updateDentist(Integer id);

    void deleteDentist(Integer id);

}
