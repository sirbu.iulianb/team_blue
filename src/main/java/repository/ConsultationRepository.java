package repository;

import model.Consultation;

import java.util.List;

public interface ConsultationRepository {

    void save(Consultation consultation);

    List<Consultation> findAll();

    Consultation findConsultationById(Integer id);

    void updateConsultation(Integer id, Consultation newConsultation);

    void updateConsultationDateById(Integer id);

    void deleteConsultation(Integer id);
}
