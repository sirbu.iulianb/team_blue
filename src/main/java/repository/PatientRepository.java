package repository;

import model.Patient;

import java.util.List;

public interface PatientRepository {

    Integer save(Patient patient);

    List<Patient> findAll();

    Patient findPatientById(Integer id);

    void updatePatient(Integer id, Patient newPatient);

    void updatePatient(Integer id);

    void deletePatient(Integer id);
}
