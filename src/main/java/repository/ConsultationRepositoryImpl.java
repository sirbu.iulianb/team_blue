package repository;

import config.ConnectToDatabase;
import model.Consultation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ConsultationRepositoryImpl implements ConsultationRepository {
    @Override
    public void save(Consultation consultation) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.save(consultation);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Consultation> findAll() {
        try {
            ConnectToDatabase connect = new ConnectToDatabase();
            Session session = connect.getSessionFactory().openSession();
            Query query = session.createQuery("from model.Consultation", Consultation.class);
            return query.list();
        } catch (Exception e) {
            System.out.println(e);
        }
        return Collections.emptyList();
    }

    @Override
    public Consultation findConsultationById(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            System.out.println(session.find(Consultation.class, id));
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }


    @Override
    public void updateConsultation(Integer id, Consultation newConsultation) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            Consultation oldConsultation = session.find(Consultation.class, id);

            oldConsultation.setPatientId(newConsultation.getPatientId());
            oldConsultation.setDentistId(newConsultation.getDentistId());
            oldConsultation.setDate(newConsultation.getDate());

            connection.getSession().update(newConsultation);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void updateConsultationDateById(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Consultation consultation = connection.getSession().find(Consultation.class, id);
            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce new consultation date");
            String consultationDate = scanner.next().trim();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate parsedDate = LocalDate.parse(consultationDate, formatter);
            consultation.setDate(parsedDate);

            connection.getSession().update(consultation);
            connection.getTransaction().commit();
            connection.getSession().close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    @Override
    public void deleteConsultation(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();

            Consultation consultation = session.find(Consultation.class, id);
            session.delete(consultation);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
