package repository;

import config.ConnectToDatabase;
import model.Gender;
import model.Patient;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PatientRepositoryImpl implements PatientRepository {

    @Override
    public Integer save(Patient patient) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            Integer id = (Integer) session.save(patient);
            transaction.commit();
            session.close();
            return id;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }


    @Override
    public List<Patient> findAll() {
        try {
            ConnectToDatabase connect = new ConnectToDatabase();
            Session session = connect.getSessionFactory().openSession();
            Query<Patient> query = session.createQuery("from model.Patient", Patient.class);
            return query.list();
        } catch (Exception e) {
            System.out.println(e);
        }
        return Collections.emptyList();
    }

    @Override
    public Patient findPatientById(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            return session.find(Patient.class, id);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public void updatePatient(Integer id, Patient newPatient) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();

            Patient oldPatient = session.find(Patient.class, id);
            oldPatient.setFirstName(newPatient.getFirstName());
            oldPatient.setLastName(newPatient.getLastName());
            oldPatient.setGender(newPatient.getGender());
            oldPatient.setAge(newPatient.getAge());
            oldPatient.setAddress(newPatient.getAddress());
            oldPatient.setPhone(newPatient.getPhone());
            connection.getSession().update(newPatient);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void updatePatient(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Patient patient = connection.getSession().find(Patient.class, id);

            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce new patient first name:");
            String firstName = scanner.nextLine();
            System.out.println("Introduce new patient last name");
            String lastName = scanner.nextLine();
            lastName += scanner.nextLine();
            System.out.println("Select the new patient gender (Type M or F");
            String gender = scanner.next();
            Gender gen = genderIgnoreCase(gender);
            System.out.println("Introduce new  patient age");
            String age = scanner.next().trim();
            Integer parsedAge = Integer.parseInt(age);
            System.out.println("Introduce new patient address");
            String address = scanner.nextLine();
            address += scanner.nextLine();
            System.out.println("Introduce new patient phone number");
            String phone = scanner.next();

            patient.setFirstName(firstName);
            patient.setLastName(lastName);
            patient.setGender(gen);
            patient.setAge(parsedAge);
            patient.setAddress(address);
            patient.setPhone(phone);

            connection.getSession().update(patient);
            connection.getTransaction().commit();
            connection.getSession().close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private Gender genderIgnoreCase(String gender) {
        Gender gen;
        if (gender.equalsIgnoreCase("m")) {
            gen = Gender.M;
        } else {
            gen = Gender.F;
        }
        return gen;
    }


    @Override
    public void deletePatient(Integer id) {
        try {
            ConnectToDatabase connection = new ConnectToDatabase();
            connection.invoke();
            Session session = connection.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();

            Patient patient = session.find(Patient.class, id);
            session.delete(patient);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }


}

