import model.Consultation;
import model.Dentist;
import model.Gender;
import model.Patient;
import config.ConnectToDatabase;
import repository.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

   static PatientRepository patientRepository;
   static DentistRepository dentistRepository;
   static ConsultationRepository consultationRepository;
   static Scanner scanner;

    public static void main(String[] args) {
        ConnectToDatabase connectToDatabase = new ConnectToDatabase();
        connectToDatabase.getSessionFactory();

patientRepository = new PatientRepositoryImpl();
dentistRepository = new DentistRepositoryImpl();
consultationRepository = new ConsultationRepositoryImpl();
scanner = new Scanner(System.in);

                try {
            while (true) {

                selectEntity();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void findPatient() {
        System.out.println("What patient ID do you want to find?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        patientRepository.findPatientById(parsedId);
    }

    private static void findDentist() {
        System.out.println("What dentist ID do you want to find?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        dentistRepository.findDentistById(parsedId);
    }

    private static void findConsultation() {
        System.out.println("What consultation ID do you want to find?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        consultationRepository.findConsultationById(parsedId);
    }

    private static void deletePatient() {
        System.out.println("What patient ID do you want to delete?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        patientRepository.deletePatient(parsedId);
    }

    private static void deleteDentist() {
        System.out.println("What dentist ID do you want to delete?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        dentistRepository.deleteDentist(parsedId);
    }

    private static void deleteConsultation() {
        System.out.println("What consultation ID do you want to delete?");
        String id = scanner.next().trim();
        Integer parsedId = Integer.parseInt(id);
        consultationRepository.deleteConsultation(parsedId);
    }

    private static Patient initializePatient() {
        System.out.println("Introduce the patient first name:");
        String firstName = scanner.nextLine();
        System.out.println("Introduce the patient last name");
        String lastName = scanner.nextLine();
        System.out.println("Select whether the patient is a Male or Female (Type M or F)");
        String gender = scanner.next();
        Gender gen;
        if (gender.equalsIgnoreCase("m")) {
            gen = Gender.M;
        } else {
            gen = Gender.F;
        }
        System.out.println("Introduce the patient age");
        String age = scanner.next().trim();
        Integer parsedAge = Integer.parseInt(age);
        System.out.println("Introduce the patient address");
        String address = scanner.nextLine();
        address += scanner.nextLine();
        System.out.println("Introduce the patient phone number");
        String phone = scanner.next();
        return new Patient(firstName, lastName, gen, parsedAge, address, phone);
    }

    private static Dentist initializeDentist() {
        System.out.println("Introduce the dentist first Name:");
        String firstName = scanner.nextLine();
        System.out.println("Introduce the dentist last name");
        String lastName = scanner.nextLine();
        System.out.println("Introduce the dentist specialty");
        String specialty = scanner.next();
        return new Dentist(firstName, lastName, specialty);
    }

    private static Consultation initializeConsultation() {
        System.out.println("Introduce patient id");
        String patientId = scanner.nextLine().trim();
        Integer parsedPatientId = Integer.parseInt(patientId);

        System.out.println("Introduce dentist id");
        String dentistId = scanner.nextLine().trim();
        Integer parsedDentistId = Integer.parseInt(dentistId);
        System.out.println("Introduce affliction");
        String affliction = scanner.nextLine();
        System.out.println("Introduce consultation date");
        String consultationDate = scanner.next().trim();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate parsedDate = LocalDate.parse(consultationDate, formatter);

        if (patientRepository.findPatientById(parsedPatientId) != null) {
            if (dentistRepository.findDentistById(parsedDentistId) != null) {
                return new Consultation(parsedPatientId, parsedDentistId, affliction, parsedDate);
            }
        }
        return null;
    }

    private static void selectEntity() throws Exception {
        System.out.println("Do you want to create, to find or to delete? (Press D for delete, F to find and C for create)");
        String input = scanner.next();

        if (input.equalsIgnoreCase("D")) {
            System.out.println("P for patient - D for dentist - C for consultation - Press a key");
            String inputChoiceDelete = scanner.next();
            if (inputChoiceDelete.equalsIgnoreCase("P")) {
                deletePatient();
            } else if (inputChoiceDelete.equalsIgnoreCase("D")) {
                deleteDentist();
            } else if (inputChoiceDelete.equalsIgnoreCase("C")) {
                deleteConsultation();
            } else {
                throw new Exception();
            }
        } else if (input.equalsIgnoreCase("C")) {
            System.out.println("P for patient - D for dentist - C for consultation - Press a key");
            String inputChoiceCreate = scanner.next();

            if (inputChoiceCreate.equalsIgnoreCase("P")) {
                Patient patient = initializePatient();
                PatientRepositoryImpl patientRepositoryImpl = new PatientRepositoryImpl();
                patientRepositoryImpl.save(patient);
            } else if (inputChoiceCreate.equalsIgnoreCase("D")) {
                Dentist dentist = initializeDentist();
                DentistRepositoryImpl dentistRepositoryImpl = new DentistRepositoryImpl();
                dentistRepositoryImpl.save(dentist);
            } else if (inputChoiceCreate.equalsIgnoreCase("C")) {
                Consultation consultation = initializeConsultation();
                ConsultationRepositoryImpl consultationRepositoryImpl = new ConsultationRepositoryImpl();
                consultationRepositoryImpl.save(consultation);
            } else {
                throw new Exception();
            }
        } else if (input.equalsIgnoreCase("F")) {
            System.out.println("P for patient - D for dentist - C for consultation - Press a key");
            String inputChoiceCreate = scanner.next();
            if (inputChoiceCreate.equalsIgnoreCase("P")) {
                findPatient();
            } else if (inputChoiceCreate.equalsIgnoreCase("D")) {
                findDentist();
            } else if (inputChoiceCreate.equalsIgnoreCase("C")) {
                findConsultation();
            } else {
                throw new Exception();
            }
        }
    }
}
